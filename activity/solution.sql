-- login to MySQL
mysql -u root -p

-- Create blog_db database
CREATE DATABASE blog_db;

-- Switching to active database
USE blog_db;

-- Creating tables in a database
CREATE TABLE users (
	id INT NOT NULL AUTO_INCREMENT,
	email VARCHAR(55) NOT NULL,
	password VARCHAR(55) NOT NULL,
	datetime_created DATETIME NOT NULL,
	PRIMARY KEY (id)
);

CREATE TABLE posts (
	id INT NOT NULL AUTO_INCREMENT,
	user_id INT NOT NULL,
	title VARCHAR(55) NOT NULL,
	content VARCHAR(100) NOT NULL,
	datetime_posted DATETIME NOT NULL,
	PRIMARY KEY (id),
	CONSTRAINT fk_posts_user_id
		FOREIGN KEY (user_id) REFERENCES users(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT
);

-- [SECTION] Creating new record
-- Users table

INSERT INTO users (email, password, datetime_created) VALUES ("johnsmith@gmail.com", "passwordA", "2021-01-01 01:00:00");
INSERT INTO users (email, password, datetime_created) VALUES ("juandelacruz@gmail.com", "passwordB", "2021-01-01 02:00:00");
INSERT INTO users (email, password, datetime_created) VALUES ("janesmith@gmail.com", "passwordC", "2021-01-01 03:00:00");
INSERT INTO users (email, password, datetime_created) VALUES ("mariadelacruz@gmail.com", "passwordD", "2021-01-01 04:00:00");
INSERT INTO users (email, password, datetime_created) VALUES ("johndoe@gmail.com", "passwordE", "2021-01-01 05:00:00");

-- Posts table

INSERT INTO posts (user_id, title, content, datetime_posted) VALUES (1, "First Code", "Hello World!", "2021-01-02 1:00:00");
INSERT INTO posts (user_id, title, content, datetime_posted) VALUES (1, "Second Code", "Hello Earth!", "2021-01-02 2:00:00");
INSERT INTO posts (user_id, title, content, datetime_posted) VALUES (2, "Third Code", "Welcome to Mars!", "2021-01-02 3:00:00");
INSERT INTO posts (user_id, title, content, datetime_posted) VALUES (4, "Fourth Code", "Bye bye solar system!", "2021-01-02 4:00:00");

-- [SECTION] Activity
-- Get all the post with an Author ID of 1

SELECT * FROM posts WHERE user_id = 1;

-- Get all the user's email and datetime of creation

SELECT email, datetime_created FROM users;

-- Update a post's content to "Hello to the people of the Earth!" where its initial content is "Hello Earth!" by using the record's ID

UPDATE posts SET content = "Hello to the people of the Earth!" WHERE id = 2;

-- Delete the user with an email of "johndoe@gmail.com".

DELETE FROM users WHERE email = "johndoe@gmail.com";